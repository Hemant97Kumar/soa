# <center>Report on Service-Oriented Architecture (SOA)</center>

## Introduction

Service-Oriented Architecture is a style of [designing software](https://en.wikipedia.org/wiki/software_design "Go to software design") in which applications make use of services available in the network through a [communication protocol](https://en.wikipedia.org/wiki/Communications_protocol "Go to communication protocol"). Different services can be used in conjunction to provide the functionality of a large [software application](https://en.wikipedia.org/wiki/Software_applications "Go to software application").

## Core values of SOA

* **Business value** is given more importance than technical strategy.

* **Strategic goals** are given more importance than project-specific benefits.

* **Intrinsic interoperability** is given more importance than custom integration.

* **Shared services** are given more importance than specific-purpose implementations.

* **Flexibility** is given more importance than optimization.

* **Evolutionary refinement** is given more importance than pursuit of initial perfection.

## Components of SOA

![Image](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-248.png "SOA Components")

## Implementation approach

Service-oriented architecture can be implemented with [web services](https://en.wikipedia.org/wiki/Web_service "Go to web services") or [Microservices](https://en.wikipedia.org/wiki/Microservices "Go to Microservices"). It can use one or more protocols. SOA access the services from the internet and combine them with the software, and it can be independent of platform and programming language. 

## Benefits of SOA

* Different developers working on a project can't use different tools, But they would code with a standard set of services.

* Applications are made from existing services, can be reused to make many applications.

* Services are independent of each other, can be modified without affecting each other.

* SOA allows making a complex application by combining 
different services,  which is independent of the platform.

## References:
1. https://en.wikipedia.org/wiki/Service-oriented_architecture
2. https://www.geeksforgeeks.org/service-oriented-architecture/
